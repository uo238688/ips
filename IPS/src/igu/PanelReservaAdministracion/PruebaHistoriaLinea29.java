package igu.PanelReservaAdministracion;

import java.awt.EventQueue;

import javax.swing.JFrame;

import logica.Instalaciones.Instalacion;
import logica.Instalaciones.InstalacionBase;
import logica.Permisos.BaseDatos;

public class PruebaHistoriaLinea29 {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//BaseDatos.instance();
					PruebaHistoriaLinea29 window = new PruebaHistoriaLinea29();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PruebaHistoriaLinea29() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 347);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Instalacion ins = new InstalacionBase (""+1, "Piscina",100.0);
		frame.getContentPane().add(new Panel_Reserva_Administracion_Para_Socio(ins));
	}

}
