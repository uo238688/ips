package igu.PanelReservaAdministracion;

import java.util.Calendar;
import java.util.Date;

import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.ChangedCharSetException;

import com.toedter.calendar.JCalendar;

import logica.FranjaHorarios.FranjaHorarioInstalacion;
//import Uso.Alquiler;
import logica.Instalaciones.Instalacion;
import logica.Instalaciones.InstalacionBase;
import logica.Usos.Alquiler;
import logica.Usuarios.Socio;

import javax.swing.JButton;
import javax.swing.Action;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;

public class Panel_Reserva_Administracion_Para_Socio extends JPanel {
	
	Instalacion instalacion;
	
	public Panel_Reserva_Administracion_Para_Socio(Instalacion instalacion) {
		setLayout(null);
		add(getCalendar());
		add(getBtnReservar());
		add(getComboBox());
		add(getComboBox_1());
		add(getLblHoraInicio());
		add(getLblHoraFin());
		
		this.instalacion = instalacion;
		super.setName("Reservar " + instalacion.getNombre());
		
		ActionListener accion_comboBox = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!comboBox.getSelectedItem().equals("")) {
					comboBox_1.setModel(comboBox_1.modelo_horas((String)comboBox.getSelectedItem())); //Mirar porq no hace el casting por genericidad
					comboBox_1.setEnabled(true);
				}
			}
		};
		
		ActionListener accion_comboBox_1 = new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (!(comboBox_1.getSelectedItem().equals("") && textUsuario.getText().equals(""))) 
					btnReservar.setEnabled(true);
				
			}
			
		};
		
		ActionListener accion_reserva = new Accion_Reserva_Para_Socio(this);
		add(getLblSocio());
		add(getTextUsuario());
		
		comboBox.addActionListener(accion_comboBox);
		comboBox_1.addActionListener(accion_comboBox_1);
		
		btnReservar.addActionListener(accion_reserva);
		
		
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JCalendar calendar;
	private JButton btnReservar;
	private Combo_Horas comboBox;
	private Combo_Horas comboBox_1;
	private JLabel lblHoraInicio;
	private JLabel lblHoraFin;
	private JLabel lblSocio;
	private JTextField textUsuario;

	private JCalendar getCalendar() {
		if (calendar == null) {
			calendar = new JCalendar();
			calendar.setBounds(126, 27, 198, 145);
			
		}
		return calendar;
	}

	private JButton getBtnReservar() {
		if (btnReservar == null) {
			btnReservar = new JButton("Reservar");
			btnReservar.setEnabled(false);
			btnReservar.setBounds(177, 264, 127, 23);
		}
		return btnReservar;
	}

	private JComboBox<String> getComboBox() {
		if (comboBox == null) {
			comboBox = new Combo_Horas();
			comboBox.setEnabled(true);
			comboBox.setBounds(126, 216, 62, 24);
			comboBox.setModel(comboBox.modelo_horas()); //Cambiar cuando franjas
		}
		return comboBox;
	}

	private JComboBox<String> getComboBox_1() {
		if (comboBox_1 == null) {
			comboBox_1 = new Combo_Horas();
			comboBox_1.setEnabled(false);
			comboBox_1.setBounds(293, 216, 62, 24);
		}
		return comboBox_1;
	}

	private JLabel getLblHoraInicio() {
		if (lblHoraInicio == null) {
			lblHoraInicio = new JLabel("Hora inicio :");
			lblHoraInicio.setBounds(40, 221, 101, 15);
		}
		return lblHoraInicio;
	}

	private JLabel getLblHoraFin() {
		if (lblHoraFin == null) {
			lblHoraFin = new JLabel("Hora fin :");
			lblHoraFin.setBounds(216, 221, 70, 15);
		}
		return lblHoraFin;
	}
	
	public Alquiler getFecha () {
		Date fecha = calendar.getDate();
		int hora_inicio = comboBox.getHora();
		int hora_fin = comboBox_1.getHora();
		
		return new Alquiler( new FranjaHorarioInstalacion(fecha,hora_inicio,hora_fin), instalacion);
	}
	private JLabel getLblSocio() {
		if (lblSocio == null) {
			lblSocio = new JLabel("Id socio: ");
			lblSocio.setBounds(153, 183, 55, 22);
		}
		return lblSocio;
	}
	private JTextField getTextUsuario() {
		if (textUsuario == null) {
			textUsuario = new JTextField();
			textUsuario.setBounds(218, 183, 86, 22);
			textUsuario.setColumns(10);
		}
		return textUsuario;
	}

	public String getUsuarioId() {
		return textUsuario.getText();
	}
}
