package igu.PanelReservaAdministracion;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import logica.Permisos.BaseDatos;

//import Database.*;


public class Accion_Reserva_Para_Socio implements ActionListener {

	Panel_Reserva_Administracion_Para_Socio panel;
	
	
	
	public Accion_Reserva_Para_Socio(Panel_Reserva_Administracion_Para_Socio panel) {
		super();
		this.panel = panel;
	}



	@Override
	public void actionPerformed(ActionEvent e) {
		int opcion = JOptionPane.showConfirmDialog(panel,  "� Quiere continuar con la reserva ?" ,"", JOptionPane.YES_NO_OPTION);
		if (opcion == JOptionPane.YES_OPTION) {
			BaseDatos.reservaParaSocio(panel.getFecha(),panel.getUsuarioId());
		}
		
	}

}
