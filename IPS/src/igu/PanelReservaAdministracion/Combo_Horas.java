package igu.PanelReservaAdministracion;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

public class Combo_Horas extends JComboBox<String> {
	public Combo_Horas() {
	}
	
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	int getHora () {
		return Integer.parseInt(((String) this.getSelectedItem()).split(" : ")[0]);
	}
	

	 DefaultComboBoxModel<String> modelo_horas(){
		 String[] horas = new String[25];
		 horas[0] = ""; 
		 for (int i = 1; i < 25; i++) {
			horas[i] = "" + (i-1) +" : " + "00";
		}
		 return new DefaultComboBoxModel<>(horas);
	 }
	 
	  DefaultComboBoxModel<String> modelo_horas(String hora){
		 int hora_int = Integer.parseInt(hora.split(" : ")[0]);
		 String[] horas = new String[24-hora_int];
		 horas[0] = ""; 
		 for (int i = 1; i <= 2; i++) {
			horas[i] = "" + (i+hora_int) +" : " + "00";
		}
		 return new DefaultComboBoxModel<>(horas);
	 }

}
