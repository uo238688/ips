package igu;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PanelCalendario extends JPanel {

	private static final long serialVersionUID = 1L;
	private JPanel pnlNombreMes;
	private JLabel lblMes;
	private JPanel pnlRetroceso;
	private JPanel pnlAvance;
	private JButton btnAvance;
	private JButton btnRetroceso;
	private JPanel pnlHojaMes;
	private JPanel pnlGridDias;
	private JLabel lblM;
	private JLabel lblX;
	private JLabel lblJ;
	private JLabel lblV;
	private JLabel lblS;
	private JLabel lblD;
	private JPanel pnlM;
	private JPanel pnlX;
	private JPanel pnlJ;
	private JPanel pnlV;
	private JPanel pnlS;
	private JPanel pnlD;
	private JPanel pnlL;
	private JLabel label;
	private JButton[][] dias;
	private Calendar calendar = GregorianCalendar.getInstance();

	/**
	 * Create the panel.
	 */
	public PanelCalendario() {
		calendar.setTimeZone(TimeZone.getTimeZone("GMT+1:00"));
		setBackground(Color.WHITE);
		setLayout(new BorderLayout(0, 0));
		add(getPnlNombreMes(), BorderLayout.NORTH);
		add(getPnlRetroceso(), BorderLayout.WEST);
		add(getPnlAvance(), BorderLayout.EAST);
		add(getPnlHojaMes(), BorderLayout.CENTER);
	}
	
	private int getPrimerDiaMes(int mes, int anio) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DATE, 1);
        cal.set(Calendar.MONTH, mes);
        cal.set(Calendar.YEAR, anio);
        if(cal.get(Calendar.DAY_OF_WEEK) == 1)
        	return 6;
        else 
        	return cal.get(Calendar.DAY_OF_WEEK) - 2;
	}
	
	private int getUltimoDiaMes(int mes, int anio) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, mes);
        cal.set(Calendar.YEAR, anio);
        cal.set(Calendar.DATE, cal.getMaximum(Calendar.DAY_OF_MONTH));
        if(cal.get(Calendar.DAY_OF_WEEK) == 1)
        	return 6;
        else 
        	return cal.get(Calendar.DAY_OF_WEEK) - 2;
	}
	
	private int getDiaSemana(int dia, int mes, int anio) {
		Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, mes);
        cal.set(Calendar.YEAR, anio);
        cal.set(Calendar.DATE, dia);
        if(cal.get(Calendar.DAY_OF_WEEK) == 1)
        	return 6;
        else 
        	return cal.get(Calendar.DAY_OF_WEEK) - 2;
	}
	
	private int getUltimoMes(int mes, int anio) {
		Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, mes);
        cal.set(Calendar.YEAR, anio);
        return cal.getActualMaximum(Calendar.DATE);
	}
	
	private void cargarBotonesMatriz(int mes, int anio) {
		calendar.set(Calendar.MONTH, mes);
		calendar.set(Calendar.YEAR, anio);
		int diaSemanaI = getPrimerDiaMes(calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
		int diaSemanaF = getUltimoDiaMes(calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
		int cont = 1;
		for(int s = 0; s < 6; s++) {
			for(int d = 0; d < 7; d++) {
				if(!((s == 0 && d < diaSemanaI) || (cont > getUltimoMes(calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR))))) {
					
					
					//Definir ActionListener para que haga lo que queremos
					
					
					JButton btn = new ButtonDia(calendar, null, cont);
					dias[s][getDiaSemana(cont, mes, anio)] = btn;
					cont++;
				}
			}
		}
	}

	private JPanel getPnlNombreMes() {
		if (pnlNombreMes == null) {
			pnlNombreMes = new JPanel();
			pnlNombreMes.setBackground(Color.WHITE);
			pnlNombreMes.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			pnlNombreMes.add(getLblMes());
		}
		return pnlNombreMes;
	}
	
	private String enteroAStringMes(int n) {
		String mes = "";
		switch(n) {
			case 0 : mes = "ENERO"; break;
			case 1 : mes = "FEBRERO"; break;
			case 2 : mes = "MARZO"; break;
			case 3 : mes = "ABRIL"; break;
			case 4 : mes = "MAYO"; break;
			case 5 : mes = "JUNIO"; break;
			case 6 : mes = "JULIO"; break;
			case 7 : mes = "AGOSTO"; break;
			case 8 : mes = "SEPTIEMBRE"; break;
			case 9 : mes = "OCTUBRE"; break;
			case 10 : mes = "NOVIEMBRE"; break;
			case 11 : mes = "DICIEMBRE"; break;
		}
		return mes;
	}
	
	private JLabel getLblMes() {
		if (lblMes == null) {
			int mes = calendar.get(Calendar.MONTH);
			int anio = calendar.get(Calendar.YEAR);
			lblMes = new JLabel(enteroAStringMes(mes) + " " + String.valueOf(anio));
			lblMes.setBackground(Color.WHITE);
		}
		return lblMes;
	}
	private void refrescarLblMes() {
		int mes = calendar.get(Calendar.MONTH);
		int anio = calendar.get(Calendar.YEAR);
		lblMes.setText(enteroAStringMes(mes) + " " + String.valueOf(anio));
		lblMes.repaint();
	}
	private JPanel getPnlRetroceso() {
		if (pnlRetroceso == null) {
			pnlRetroceso = new JPanel();
			pnlRetroceso.setBackground(Color.WHITE);
			pnlRetroceso.setLayout(new GridLayout(0, 1, 0, 0));
			pnlRetroceso.add(getBtnRetroceso());
		}
		return pnlRetroceso;
	}
	private JPanel getPnlAvance() {
		if (pnlAvance == null) {
			pnlAvance = new JPanel();
			pnlAvance.setBackground(Color.WHITE);
			pnlAvance.setLayout(new GridLayout(0, 1, 0, 0));
			pnlAvance.add(getBtnAvance());
		}
		return pnlAvance;
	}
	private JButton getBtnAvance() {
		if (btnAvance == null) {
			btnAvance = new JButton(">");
			btnAvance.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if(calendar.get(Calendar.MONTH) < 11)
						calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1);
					else {
						calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) + 1);
						calendar.set(Calendar.MONTH, 0);
					}
					refrescarCalendario();
				}
			});
			btnAvance.setBorder(new EmptyBorder(0, 10, 0, 10));
			btnAvance.setBackground(Color.WHITE);
		}
		return btnAvance;
	}
	private void refrescarCalendario() {
		refrescarLblMes();
		refrescarPnlGridDias();
		this.repaint();
	}
	private JButton getBtnRetroceso() {
		if (btnRetroceso == null) {
			btnRetroceso = new JButton("<");
			btnRetroceso.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if(calendar.get(Calendar.MONTH) > 0)
						calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
					else {
						calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 1);
						calendar.set(Calendar.MONTH, 11);
					}
					refrescarCalendario();
				}
			});
			btnRetroceso.setBorder(new EmptyBorder(0, 10, 0, 10));
			btnRetroceso.setBackground(Color.WHITE);
		}
		return btnRetroceso;
	}
	private JPanel getPnlHojaMes() {
		if (pnlHojaMes == null) {
			pnlHojaMes = new JPanel();
			pnlHojaMes.setBackground(Color.WHITE);
			pnlHojaMes.setLayout(new BorderLayout(0, 0));
			pnlHojaMes.add(getPnlGridDias());
		}
		return pnlHojaMes;
	}
	
	private void insertarEnPanelGridDias() {
		for(int i = 0; i < 6; i++) {
			for(int j = 0; j < 7; j++) {
				if(dias[i][j] != null) {
					GridBagConstraints gbc_pnl = new GridBagConstraints();
					gbc_pnl.insets = new Insets(0, 0, 5, 5);
					gbc_pnl.fill = GridBagConstraints.BOTH;
					gbc_pnl.gridx = j;
					gbc_pnl.gridy = i + 1;
					pnlGridDias.add(dias[i][j], gbc_pnl);
				}
			}
		}
	}
	
	private void borrarEnPanelGridDias() {
		int cont = 0;
		for(Component c : pnlGridDias.getComponents()) {
			if(cont > 6)
				pnlGridDias.remove(c);
			cont++;
		}
	}
	
	private void refrescarPnlGridDias() {
		borrarEnPanelGridDias();
		dias = new JButton[6][7];
		cargarBotonesMatriz(calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
		insertarEnPanelGridDias();
	}
	
	private JPanel getPnlGridDias() {
		dias = new JButton[6][7];
		cargarBotonesMatriz(calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
		if (pnlGridDias == null) {
			pnlGridDias = new JPanel();
			pnlGridDias.setBackground(Color.WHITE);
			pnlGridDias.setBorder(new EmptyBorder(20, 20, 20, 20));
			GridBagLayout gbl_pnlGridDias = new GridBagLayout();
			gbl_pnlGridDias.columnWidths = new int[]{57, 57, 57, 57, 57, 57, 57};
			gbl_pnlGridDias.rowHeights = new int[] {57, 57, 57, 57, 57, 57, 57};
			gbl_pnlGridDias.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
			gbl_pnlGridDias.rowWeights = new double[]{0.0};
			pnlGridDias.setLayout(gbl_pnlGridDias);
			GridBagConstraints gbc_pnlL = new GridBagConstraints();
			gbc_pnlL.fill = GridBagConstraints.BOTH;
			gbc_pnlL.insets = new Insets(0, 0, 0, 5);
			gbc_pnlL.gridx = 0;
			gbc_pnlL.gridy = 0;
			pnlGridDias.add(getPnlL(), gbc_pnlL);
			GridBagConstraints gbc_pnlM = new GridBagConstraints();
			gbc_pnlM.fill = GridBagConstraints.BOTH;
			gbc_pnlM.insets = new Insets(0, 0, 0, 5);
			gbc_pnlM.gridx = 1;
			gbc_pnlM.gridy = 0;
			pnlGridDias.add(getPnlM(), gbc_pnlM);
			GridBagConstraints gbc_pnlX = new GridBagConstraints();
			gbc_pnlX.fill = GridBagConstraints.BOTH;
			gbc_pnlX.insets = new Insets(0, 0, 0, 5);
			gbc_pnlX.gridx = 2;
			gbc_pnlX.gridy = 0;
			pnlGridDias.add(getPnlX(), gbc_pnlX);
			GridBagConstraints gbc_pnlJ = new GridBagConstraints();
			gbc_pnlJ.fill = GridBagConstraints.BOTH;
			gbc_pnlJ.insets = new Insets(0, 0, 0, 5);
			gbc_pnlJ.gridx = 3;
			gbc_pnlJ.gridy = 0;
			pnlGridDias.add(getPnlJ(), gbc_pnlJ);
			GridBagConstraints gbc_pnlV = new GridBagConstraints();
			gbc_pnlV.fill = GridBagConstraints.BOTH;
			gbc_pnlV.insets = new Insets(0, 0, 0, 5);
			gbc_pnlV.gridx = 4;
			gbc_pnlV.gridy = 0;
			pnlGridDias.add(getPnlV(), gbc_pnlV);
			GridBagConstraints gbc_pnlS = new GridBagConstraints();
			gbc_pnlS.fill = GridBagConstraints.BOTH;
			gbc_pnlS.insets = new Insets(0, 0, 0, 5);
			gbc_pnlS.gridx = 5;
			gbc_pnlS.gridy = 0;
			pnlGridDias.add(getPnlS(), gbc_pnlS);
			GridBagConstraints gbc_pnlD = new GridBagConstraints();
			gbc_pnlD.fill = GridBagConstraints.BOTH;
			gbc_pnlD.gridx = 6;
			gbc_pnlD.gridy = 0;
			pnlGridDias.add(getPnlD(), gbc_pnlD);
			insertarEnPanelGridDias();
		}
		return pnlGridDias;
	}
	private JLabel getLblM() {
		if (lblM == null) {
			lblM = new JLabel("M");
			lblM.setBackground(Color.WHITE);
		}
		return lblM;
	}
	private JLabel getLblX() {
		if (lblX == null) {
			lblX = new JLabel("X");
			lblX.setBackground(Color.WHITE);
		}
		return lblX;
	}
	private JLabel getLblJ() {
		if (lblJ == null) {
			lblJ = new JLabel("J");
			lblJ.setBackground(Color.WHITE);
		}
		return lblJ;
	}
	private JLabel getLblV() {
		if (lblV == null) {
			lblV = new JLabel("V");
			lblV.setBackground(Color.WHITE);
		}
		return lblV;
	}
	private JLabel getLblS() {
		if (lblS == null) {
			lblS = new JLabel("S");
			lblS.setBackground(Color.WHITE);
		}
		return lblS;
	}
	private JLabel getLblD() {
		if (lblD == null) {
			lblD = new JLabel("D");
			lblD.setBackground(Color.WHITE);
		}
		return lblD;
	}
	private JPanel getPnlM() {
		if (pnlM == null) {
			pnlM = new JPanel();
			pnlM.setBackground(Color.WHITE);
			pnlM.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			pnlM.add(getLblM());
		}
		return pnlM;
	}
	private JPanel getPnlX() {
		if (pnlX == null) {
			pnlX = new JPanel();
			pnlX.setBackground(Color.WHITE);
			pnlX.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			pnlX.add(getLblX());
		}
		return pnlX;
	}
	private JPanel getPnlJ() {
		if (pnlJ == null) {
			pnlJ = new JPanel();
			pnlJ.setBackground(Color.WHITE);
			pnlJ.add(getLblJ());
		}
		return pnlJ;
	}
	private JPanel getPnlV() {
		if (pnlV == null) {
			pnlV = new JPanel();
			pnlV.setBackground(Color.WHITE);
			pnlV.add(getLblV());
		}
		return pnlV;
	}
	private JPanel getPnlS() {
		if (pnlS == null) {
			pnlS = new JPanel();
			pnlS.setBackground(Color.WHITE);
			pnlS.add(getLblS());
		}
		return pnlS;
	}
	private JPanel getPnlD() {
		if (pnlD == null) {
			pnlD = new JPanel();
			pnlD.setBackground(Color.WHITE);
			pnlD.add(getLblD());
		}
		return pnlD;
	}
	private JPanel getPnlL() {
		if (pnlL == null) {
			pnlL = new JPanel();
			pnlL.setBackground(Color.WHITE);
			pnlL.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			pnlL.add(getLabel());
		}
		return pnlL;
	}
	private JLabel getLabel() {
		if (label == null) {
			label = new JLabel("L");
			label.setBackground(Color.WHITE);
		}
		return label;
	}
}
