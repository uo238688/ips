package igu;

import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;

public class ButtonDia extends JButton {
	
	private int diaMes;
	private static final long serialVersionUID = 1L;
	
	public ButtonDia(Calendar calendar, ActionListener acListen, int diaMes) {
		setBorder(null);
		this.diaMes = diaMes;
		setText(String.valueOf(this.diaMes));
		setBackground(Color.WHITE);
		this.addActionListener(acListen);
	}


}
