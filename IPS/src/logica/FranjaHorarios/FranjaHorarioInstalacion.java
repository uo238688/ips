package logica.FranjaHorarios;

import java.util.Date;

import logica.Usos.Uso;

public class FranjaHorarioInstalacion extends AbstractFranjaHorario {
	
	private Uso uso;

	public FranjaHorarioInstalacion(Date fecha, int horaInicio, int horaFin) {
		this.fecha = fecha;
		this.horaInicio = horaInicio;
		this.horaFin = horaFin;
		this.uso = null;//corregir esto en la base de datos
	}
}
