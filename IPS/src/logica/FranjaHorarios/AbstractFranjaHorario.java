package logica.FranjaHorarios;

import java.util.Date;

public abstract class AbstractFranjaHorario implements FranjaHorario {

	protected int horaInicio;
	protected int horaFin;
	protected Date fecha;
	
	public int getHoraInicio() {
		return horaInicio;
	}
	
	public int getHoraFin() {
		return horaFin;
	}
	
	public Date getFecha() {
		return fecha;
	}
}
