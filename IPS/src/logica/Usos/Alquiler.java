package logica.Usos;

import java.util.Date;

import logica.FranjaHorarios.FranjaHorarioInstalacion;
import logica.Instalaciones.Instalacion;
import logica.Instalaciones.InstalacionBase;

public class Alquiler extends AbstractUso {
	
	FranjaHorarioInstalacion franja;
	
	Instalacion instalacion;
	public String estado;

	public Alquiler(FranjaHorarioInstalacion franja, Instalacion instalacion) {
		super();
		this.franja = franja;
		this.instalacion = instalacion;
		this.estado="pendiente";
	}

	public Date getFecha() {
		return franja.getFecha();
	}

	public int getHora_inicio() {
		return franja.getHoraInicio();
	}

	public int getHora_fin() {
		return franja.getHoraFin();
	}

	public String getId_instalacion() {
		return instalacion.getIdInstalacion();
	}
	
	public double getPrecio_instalacion() {
		return instalacion.getPrecio();
	}
	
	public void pagoPendiente() {
		estado="pendiente";
	}
	
	public void pagoPagado() {
		estado="pagado";
	}
	
	

}
