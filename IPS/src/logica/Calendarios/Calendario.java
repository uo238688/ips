package logica.Calendarios;

import java.util.List;

import logica.FranjaHorarios.FranjaHorario;
import logica.Instalaciones.Instalacion;

public class Calendario implements FranjaHorario {

	protected List<FranjaHorario> franjas;
	
	public void setFranjas(List<FranjaHorario> franjas) {
		this.franjas = franjas;
	}
	
	public List<FranjaHorario> getFranjas(Instalacion instalacion, String fechaInicio, String fechaFin) {
		return franjas;
	}
}
