package logica;

import logica.Usuarios.Usuario;
import logica.Usuarios.UsuarioBase;

public class GestorUsuarios {

	private Usuario usuario;
	private static GestorUsuarios instanciaGestor = new GestorUsuarios();
	
	//Clase que se ocupa del login, es un singleton. Es una factor�a
	//Crea los usuarios (Socio, Contable...)
	
	private GestorUsuarios() {
		usuario = new UsuarioBase();
	}
	
	//getInstance del Singleton
	public static GestorUsuarios comprobarCredenciales(String nombre, String pass) {
		//Comprobar...if...
		//Crear usuario
		//usuario = new Usuario();
		return instanciaGestor;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
}
