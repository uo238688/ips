package logica.Instalaciones;

public interface Instalacion {

	public String getIdInstalacion();
	
	public String getNombre();
	
	public double getPrecio();
	
}
