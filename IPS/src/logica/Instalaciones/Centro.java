package logica.Instalaciones;

import java.util.List;

import logica.GestorUsuarios;
import logica.Calendarios.Calendario;
import logica.Calendarios.CalendarioCentro;
import logica.Calendarios.CalendarioInstalacion;
import logica.FranjaHorarios.FranjaHorario;
import logica.Permisos.Permiso;

public class Centro{

	private GestorUsuarios gestor;
	private List<Instalacion> instalaciones;
	
	public Centro(String idInstalacion, String nombre) {
		//super(idInstalacion, nombre);
		gestor = GestorUsuarios.comprobarCredenciales("", "");//modificar al iniciar sesiones de usuario
		Calendario calendario = new CalendarioCentro();
		instalaciones = getPermiso().getInstalaciones();
	}
	
	public Calendario getCalendarioInstalacion(Instalacion instalacion, String fechaInicio, String fechaFin) {
		List<FranjaHorario> franjas = getPermiso().getReservasInstalacion(instalacion, fechaInicio, fechaFin);
		return new CalendarioInstalacion(franjas);
	}
	
	public void iniciarSesion(String nombre, String pass) {
		gestor = GestorUsuarios.comprobarCredenciales(nombre, pass);
	}
	
	private Permiso getPermiso() {
		return gestor.getUsuario().getPermiso();
	}
	
	public Instalacion getInstalacion(String idInstalacion) {
		for(Instalacion i : instalaciones) {
			if(i.getIdInstalacion().equals(idInstalacion))
				return i;
		}
		return null;
	}
	
	//Resto de funcionalidades
}
