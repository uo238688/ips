package logica.Instalaciones;

import logica.Calendarios.Calendario;
import logica.Usuarios.Usuario;

public class InstalacionBase implements Instalacion{

	protected String idInstalacion;
	protected String nombre;
	protected Calendario calendario;
	protected Usuario usuarioReserva;
	protected double precio;
	
	public InstalacionBase(String idInstalacion, String nombre, double d) {
		this.idInstalacion = idInstalacion;
		this.nombre = nombre;
		this.precio=d;
	}
	
	@Override
	public String getIdInstalacion() {
		return idInstalacion;
	}
	
	@Override
	public boolean equals(Object o) {
		if(!(o instanceof Instalacion)) return false;
		Instalacion i = (Instalacion)o;
		if(this.idInstalacion.equals(i.getIdInstalacion())) return true;
		return false;
	}
	
	public Usuario getUsuario() {
		return usuarioReserva;
	}
	
	public String getNombre() {
		return nombre;
	}

	@Override
	public double getPrecio() {
		return precio;
	}

}
