package logica.Permisos;

import logica.Instalaciones.InstalacionBase;
import logica.Usos.Cursillo;

public class PermisoBasico extends Permiso {
	
	@Override
	public boolean apuntarAcursillo(Cursillo cursillo) {
		return bd.apuntarAcursillo(cursillo);
	}
	
	@Override
	public boolean alquilar(InstalacionBase instalacion) {
		return bd.alquilar(instalacion);
	}
}
