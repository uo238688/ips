package logica.Permisos;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;
import junit.framework.Assert;
import logica.FranjaHorarios.FranjaHorarioInstalacion;
import logica.Instalaciones.InstalacionBase;
import logica.Usos.Alquiler;
import logica.Usuarios.NoSocio;


public class DatabaseTest {

	/**
	@Test
	public void test_Reserva() {
		Database.instance();
		Reserva compuesto = new Reserva(Calendar.getInstance().getTime(), 8, 10, 1);
		Database.Introducir_Reserva(compuesto);
		Database.Ver_Reservas();
		Database.Eliminar_Reservas(compuesto);
		Database.Ver_Reservas();
	}*/
	
	@Test
	public void test_Alquiler() {
		
		NoSocio nosocio = new NoSocio("12345A", "Isaac Babor","11811");
		Alquiler alquiler = new Alquiler(new FranjaHorarioInstalacion(new Date(15, 10, 2017), 7, 9), new InstalacionBase(""+ 1,"Instalacion1",20.0));		
		PermisoSecretaria permisoSecretaria = new PermisoSecretaria();
		BaseDatos base = new BaseDatos();
		String antes = base.Ver_Reservas();
		permisoSecretaria.alquilar(alquiler, nosocio);
		String despues = base.Ver_Reservas();
		base.Eliminar_Reservas(alquiler);
		base.Eliminar_No_Socio(nosocio);
		String fin = base.Ver_Reservas();
		
		Assert.assertEquals(antes, fin);
		
	}


}
