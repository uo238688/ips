package logica.Permisos;

import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

import logica.FranjaHorarios.FranjaHorario;
import logica.FranjaHorarios.FranjaHorarioInstalacion;
import logica.Instalaciones.Instalacion;
import logica.Instalaciones.InstalacionBase;
import logica.Usos.Actividad;
import logica.Usos.Alquiler;
import logica.Usos.Cursillo;
import logica.Usuarios.NoSocio;
import logica.Usuarios.Socio;
import logica.Usuarios.Usuario;
import logica.Usuarios.UsuarioBase;

import java.util.Date;

public class BaseDatos {

	private static Connection connection;
	//private static boolean conectado;

	private static Connection getConnection() {

		//if (!conectado) {

			Connection connection = null;
			try {
				DriverManager.registerDriver(new org.hsqldb.jdbcDriver());
				connection = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/", "SA", "");
				//conectado = true;
				return connection;
			} catch (SQLException e) {
				e.printStackTrace();
				return connection;
			}
		//}
		//return connection;
	}

	public Usuario login(String nombre, String pass) {
		// Modificar cuando se cren sesiones de usuario registrado
		return new UsuarioBase();
	}

	public List<FranjaHorario> getReservasInstalacion(Instalacion instalacion, String fechaInicio, String fechaFin) {
		List<FranjaHorario> franjas = new ArrayList<FranjaHorario>();
		try {
			String sql = "select * " + "from reserva " + "where id_instalacion=? and fecha between ? and ?";
			PreparedStatement pst = getConnection().prepareStatement(sql);
			String id = instalacion.getIdInstalacion();
			pst.setString(1, id);
			pst.setString(2, fechaInicio);
			pst.setString(3, fechaFin);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				Date fecha = rs.getDate("fecha");
				int hInicio = rs.getInt("hora_inicio");
				int hFinal = rs.getInt("hora_final");
				FranjaHorario fh = new FranjaHorarioInstalacion(fecha, hInicio, hFinal);
				franjas.add(fh);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return franjas;
	}

	public List<FranjaHorario> getReservasInstalacionSinFecha(Instalacion instalacion, String fechaInicio) {
		List<FranjaHorario> franjas = new ArrayList<FranjaHorario>();
		try {
			String sql = "select * " + "from reserva " + "where id_instalacion=? and fecha>=?";
			PreparedStatement pst = getConnection().prepareStatement(sql);
			String id = instalacion.getIdInstalacion();
			pst.setString(1, id);
			pst.setString(2, fechaInicio);
			;
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				Date fecha = rs.getDate("fecha");
				int hInicio = rs.getInt("hora_inicio");
				int hFinal = rs.getInt("hora_final");
				FranjaHorario fh = new FranjaHorarioInstalacion(fecha, hInicio, hFinal);
				franjas.add(fh);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return franjas;
	}

	public List<Instalacion> cargarInstalaciones() {
		List<Instalacion> instalaciones = new ArrayList<Instalacion>();
		try {
			String sql = "select * " + "from instalacion";
			Statement st = getConnection().createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				String nombre = rs.getString("nombre_instalacion");
				String id = rs.getString("id_instalacion");
				double prec=rs.getDouble("coste_instalacion_hora");
				instalaciones.add(new InstalacionBase(id, nombre,prec));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return instalaciones;
	}

	public boolean apuntarAactividad(Actividad actividad) {
		// SQL
		return false;
	}

	public boolean apuntarAcursillo(Cursillo cursillo) {
		// SQL
		return false;
	}

	public boolean alquilar(InstalacionBase instalacion) {
		return false;
	}
	
	public static boolean alquilar (Alquiler alquiler) {
		String query = "INSERT INTO RESERVA (id_instalacion, fecha, hora_inicio, hora_final) VALUES (?, ? ,?, ?)";
		
		try {
			PreparedStatement ps = getConnection().prepareStatement(query);
			int id_instalacion = Integer.parseInt(alquiler.getId_instalacion());
			java.sql.Date sql_fecha = new java.sql.Date(alquiler.getFecha().getTime());
			//java.sql.Date sql_fecha = new java.sql.Date(alquiler.getFecha());

			
			ps.setInt(1, id_instalacion);
			ps.setDate(2, sql_fecha);
			ps.setInt(3, alquiler.getHora_inicio());
			ps.setInt(4, alquiler.getHora_fin());
			
			ps.executeUpdate();
			ps.close();
			return true; 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
			
	}
	
	

	public boolean alquilar(Alquiler alquiler, UsuarioBase usuario) {
		//Cambiar cuando triggers
		String query = "INSERT INTO RESERVA ( id_instalacion, fecha, hora_inicio, hora_fin) VALUES (?, ? ,?, ?, ?)"
				+ "INSERT INTO Alquiler (id_usuario, id_instalacion, fecha, hora_inicio)  VALUES ( ? , ? , ? , ?)";
		
		try {
			PreparedStatement ps = this.getConnection().prepareStatement(query);
			
			String id_usuario = usuario.getId_usuario();
			int id_instalacion = Integer.parseInt(alquiler.getId_instalacion());
			java.sql.Date sql_fecha = (java.sql.Date) new Date(alquiler.getFecha().getDate());

			
			ps.setInt(1, id_instalacion);
			ps.setDate(2, sql_fecha);
			ps.setInt(3, alquiler.getHora_inicio());
			ps.setInt(4, alquiler.getHora_fin());
			
			ps.setString(5, id_usuario);
			ps.setInt(6, id_instalacion);
			ps.setDate(7, sql_fecha);
			ps.setInt(8, alquiler.getHora_inicio());
			
			ps.executeUpdate();
			ps.close();
			return true; 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	
	 void Eliminar_Reservas(Alquiler alquiler) {
		String query = "DELETE FROM Reserva WHERE fecha = ? and hora_inicio = ? "
				+ "and hora_final = ? and id_instalacion = ?";

		try {
			PreparedStatement ps = this.getConnection().prepareStatement(query);
			java.sql.Date sql_fecha = (java.sql.Date) new Date(alquiler.getFecha().getDate());
			ps.setDate(1, sql_fecha);
			ps.setInt(2, alquiler.getHora_inicio());
			ps.setInt(3, alquiler.getHora_fin());
			ps.setInt(4, Integer.parseInt(alquiler.getId_instalacion()));
			ps.executeUpdate();

			ps.close();

		} catch (SQLException e) {
			System.err.println("Database.Eliminar_reserva ");
			e.printStackTrace();
		}

	}

	boolean isNosocio(NoSocio usuario) {
		boolean esta = false;
		String query = "Select * from No_Socio where id_usuario = ? ";
		PreparedStatement ps;
		try {
			ps = this.getConnection().prepareStatement(query);
			ps.setString(1, usuario.getId_usuario());

			if (ps.executeQuery().getRow() > 0)
				esta = true;
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return esta;

	}

	boolean Introducir_No_Socio(NoSocio nosocio) {
		String query = "Insert into Usuario (id_usuario, nombre_usuario) values (? , ?) ;"
				+ "Insert into No_Socio (?)";
		
		try {
			PreparedStatement ps = this.getConnection().prepareStatement(query);

			ps.setString(1, nosocio.getId_usuario());
			ps.setString(2, nosocio.getNombre());
			ps.setString(3, nosocio.getId_usuario());
			ps.executeUpdate();
			ps.close();
			return true;
		} 	catch (SQLException e) {
			System.err.println("Database.Introducir_Usuario");
			return false;
		}

	}
	
	boolean Eliminar_No_Socio (NoSocio nosocio) {
		String query = "Delete from Usuario where id_usuario= ?";
		
		try {
			PreparedStatement ps = this.getConnection().prepareStatement(query);
			
			ps.setString(1, nosocio.getId_usuario());
			ps.executeUpdate();
			ps.close();
			return true;
		} catch (SQLException e) {
			System.err.println("Database.No_socio");
			return false;
		}
		
	}
	
	 String Ver_Reservas() {
		String query = "Select fecha , hora_inicio, hora_final, nombre_instalacion From  Reserva NATURAL JOIN Instalacion";
		
		String reservas ="";
		
		try {
			Statement ps =  this.getConnection().createStatement();
			ResultSet rs = ps.executeQuery(query);

			while (rs.next()) {
				reservas += (rs.getDate(1) + " " + rs.getInt(2) + " " + rs.getInt(3) + " " + rs.getString(4));
			}

			ps.close();

		} catch (SQLException e) {
			System.err.println("Database.Introducir_reserva ");
			e.printStackTrace();
		}
		return reservas;

	}

	public static boolean reservaParaSocio(Alquiler alquiler,String usr) {
		//la reserva es de dos horas como mucho
				if(alquiler.getHora_fin()-alquiler.getHora_inicio()>2) {
					return false;
				}
				
				//se hace antes de 7 dias
				//DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				//java.sql.Date sql_fecha = (java.sql.Date) new Date(alquiler.getFecha().getDate());
				java.sql.Date sql_fecha = new java.sql.Date(alquiler.getFecha().getTime());
				
				LocalDate hoy=LocalDate.now();
				Date dateHoy = Date.from(hoy.atStartOfDay(ZoneId.systemDefault()).toInstant());
				long diff = alquiler.getFecha().getTime() - dateHoy.getTime();
			    long dias= TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
			    
			    if(dias==0 && diff<0) {
			    	System.out.println("Fecha incorrecta");
			    	return false;
			    }
			    
			    if(dias>7) {
			    	System.out.println("Debe seleccionar una fecha con menos de 7 dias de antelación");
			    	return false;
			    }
			    
			    //comprobar que la instalacion esta disponible
			    try {
					String sql = "select * "
							+ "from reserva "
							+ "where id_instalacion=? and fecha=?";
					PreparedStatement pst = getConnection().prepareStatement(sql);
					String id = alquiler.getId_instalacion();
					pst.setString(1, id);
					pst.setDate(2, sql_fecha);
					ResultSet rs = pst.executeQuery();
					while(rs.next()) {
						int hInicio = rs.getInt("hora_inicio");
						int hFinal = rs.getInt("hora_final");
						if((alquiler.getHora_inicio()>=hInicio && alquiler.getHora_inicio()<hFinal)) {
							System.out.println("Instalación ocupada");
							rs.close();
							pst.close();
							return false;
							
						}
						if(!(alquiler.getHora_inicio()<hInicio && alquiler.getHora_fin()>hInicio)) {
							System.out.println("Instalación ocupada");
							rs.close();
							pst.close();
							return false;
						}
					}
					rs.close();
					pst.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			    
			    //comprobar que es socio
			    try {
					String sql = "select * "
							+ "from socio "
							+ "where id_usuario=?";
					PreparedStatement pst = getConnection().prepareStatement(sql);
					pst.setString(1, usr);
					ResultSet rs = pst.executeQuery();
					if(!rs.next()) {
						System.out.println("No existe un socio con ese id");
						rs.close();
						pst.close();
						return false;
					}
					rs.close();
					pst.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			    
			    //comprobar que el socio no tiene otra reserva
			    try {
					String sql = "select * "
							+ "from alquiler "
							+ "where id_usuario=?";
					PreparedStatement pst = getConnection().prepareStatement(sql);
					pst.setString(1, usr);
					ResultSet rs = pst.executeQuery();
					while(rs.next()) {
						System.out.println("El socio ya tiene una reserva");
						rs.close();
						pst.close();
						return false;
					}
					rs.close();
					pst.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			    
			    
				// inserto la reserva y el alquiler
				
				try {
					String sql = "insert into reserva " + 
							"values (?,?,?,?,?)";
					String sql2 = "insert into alquiler " + 
							"values(?,?,?,?,?,?);";
					PreparedStatement pst = getConnection().prepareStatement(sql);
					PreparedStatement pst2 = getConnection().prepareStatement(sql2);
					//String id = alquiler.getId_instalacion();
					pst.setDate(1, sql_fecha);
					pst.setInt(2, alquiler.getHora_inicio());
					pst.setInt(3, alquiler.getHora_fin());
					pst.setString(4, alquiler.getId_instalacion());
					//is a
					pst.setString(5, "pendiente");
					
					pst.executeUpdate();
					pst.close();
					
					pst2.setString(1, usr);
					pst2.setDate(2, sql_fecha);
					pst2.setInt(3, alquiler.getHora_inicio());
					pst2.setString(4, alquiler.getId_instalacion());
					//coste
					pst2.setDouble(5, alquiler.getPrecio_instalacion());
					//estado( lo de pago)
					pst2.setString(6, alquiler.estado);
					
					pst2.executeUpdate();
					pst2.close();
					return true;
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
				return false;
		
	}
	
	/*
	boolean cobrarDeudas() {
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    Date date = dateFormatter .parse("2012-07-06 13:05:45");

	    //Now create the time and schedule it
	    Timer timer = new Timer();

	    //Use this if you want to execute it once
	    timer.schedule(//lo que se haga, date);
	}*/
	

}
