package logica.Permisos;

import java.util.List;

import logica.FranjaHorarios.FranjaHorario;
import logica.Instalaciones.Instalacion;
import logica.Instalaciones.InstalacionBase;
import logica.Usos.Actividad;
import logica.Usos.Cursillo;
import logica.Usuarios.Usuario;

public class Permiso {
	
	protected BaseDatos bd = new BaseDatos();
	
	public List<FranjaHorario> getReservasInstalacion(Instalacion instalacion, String fechaInicio, String fechaFin) {
		return bd.getReservasInstalacion(instalacion, fechaInicio, fechaFin);
	}
	
	public Usuario login(String nombre, String pass) {
		return bd.login(nombre, pass);
	}
	
	public List<Instalacion> getInstalaciones() {
		return bd.cargarInstalaciones();
	}
	
	public boolean apuntarAcursillo(Cursillo cursillo) {
		return false;
	}
	
	public boolean alquilar(InstalacionBase instalacion) {
		return false;
	}
	
	public boolean apuntarAactividad(Actividad actividad) {
		return false;
	}
	
	//Resto de m�todos
}
