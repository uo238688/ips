package logica.Permisos;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import logica.Instalaciones.InstalacionBase;
import logica.Usos.Alquiler;
import logica.Usuarios.NoSocio;

public class PermisoSecretaria extends Permiso {
	 
	
	public boolean alquilar (Alquiler alquiler, NoSocio usuario) {
		
		if(bd.isNosocio(usuario))
			bd.Introducir_No_Socio(usuario);	
		return bd.alquilar(alquiler, usuario);
	}
	
	public boolean alquilar (Alquiler alquiler) {
		return bd.alquilar(alquiler);
	}


	
	


}
