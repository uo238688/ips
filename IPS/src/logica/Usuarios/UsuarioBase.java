package logica.Usuarios;

import logica.Permisos.Permiso;

public class UsuarioBase extends AbstractUsuario {
	
	String id_usuario;
	String nombre;
	String telefono;
	
	public UsuarioBase() {
		super();
		permiso = new Permiso();
	}

	public UsuarioBase(String id_usuario, String nombre, String telefono) {
		super();
		this.id_usuario = id_usuario;
		this.nombre = nombre;
		this.telefono = telefono;
	}

	public String getId_usuario() {
		return id_usuario;
	}

	public String getNombre() {
		return nombre;
	}

	public String getTelefono() {
		return telefono;
	}
	
	
	
	
	


}
