package logica.Usuarios;

import logica.Permisos.Permiso;

public abstract class AbstractUsuario implements Usuario {

	protected Permiso permiso;
	
	@Override
	public Permiso getPermiso() {
		return permiso;
	}	
	
	
}
