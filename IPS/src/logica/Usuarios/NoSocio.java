package logica.Usuarios;

import logica.Permisos.PermisoBasico;
import logica.Usos.Cursillo;

public class NoSocio extends UsuarioBase {

	private PermisoBasico permisos;

	public NoSocio() {
		setUsuarioBD();
	}
	
	

	public NoSocio(String id_usuario, String nombre, String telefono) {
		super(id_usuario, nombre, telefono);
		// TODO Auto-generated constructor stub
	}



	private void setUsuarioBD() {
		permisos = new PermisoBasico();
	}

	public boolean apuntarseAcursillo(Cursillo cursillo) {
		return permisos.apuntarAcursillo(cursillo);
	}
}
