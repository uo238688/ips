package logica.Usuarios;

import logica.Permisos.PermisoSocio;
import logica.Usos.Actividad;

public class Socio extends UsuarioBase {
	
	private PermisoSocio permiso;
	
	public Socio() {
		setUsuarioBD();
	}

	private void setUsuarioBD() {
		permiso = new PermisoSocio();
	}
	
	public boolean apuntarseAactividad(Actividad actividad) {
		return permiso.apuntarAactividad(actividad);
	}
}
